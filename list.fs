: node ( addr n -- addr )
  2 CELLS ALLOCATE DROP \ Drop the 'success?' value
  DUP 2SWAP ROT 2! ;

\ Using only the node value.
: do-list ( execution-token addr -- )
  BEGIN OVER SWAP 2@ ROT EXECUTE ?DUP 0= UNTIL DROP ;

\ Using the full node value.
: do-nodes ( execution-token addr -- )
  BEGIN DUP 2@ 2SWAP OVER EXECUTE -ROT DROP ?DUP 0= UNTIL DROP ;

\ Assumes more than 1 item.
: (reduce-nodes) ( execution-token addr -- n )
  SWAP >R 2@ SWAP BEGIN
    2@ ROT R@ EXECUTE SWAP ?DUP 0=
  UNTIL R> DROP ;

: reduce-nodes ( execution-token addr -- n )
  DUP 2@ SWAP IF
    DROP (reduce-nodes)
  THEN ;

: free-node ( addr -- )
  FREE DROP ;

: .node ( addr -- )
  2@ . DROP ;

: free-list ( addr -- )
  ['] free-node SWAP do-nodes ;

: .list ( addr -- )
  ['] .node SWAP do-nodes ;

: pop ( addr -- addr n)
  DUP 2@ ROT free-node ;

: push ( addr n -- addr )
  node ;

: list ( n -- addr )
  0 SWAP node ;
