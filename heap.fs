( A simple implementation of a priority queue
  using an array backend. )

DEFER compare ( addr1 addr2 -- flag )

: heap ( n -- addr )
  DUP 2 + ALLOCATE DROP TUCK ! DUP 1 CELLS + 0 SWAP ! ;

: array ( addr -- addr )
  2 CELLS + ;

: !index ( n nindex addr -- )
  array SWAP CELLS + ! ;

: @index ( n addr -- )
  array SWAP CELLS + @ ;

: limit ( addr -- n )
  @ ;

: size ( addr -- n )
  1 CELLS + @ ;

: 1+size ( addr -- )
  1 CELLS + 1 SWAP +! ;

: size-1 ( addr -- )
  1 CELLS + -1 SWAP +! ;

: size&limit ( addr -- nsize nlimit )
  2@ ;

: in-bounds? ( addr n -- flag ) 
  SWAP size < ;

: not-full? ( addr -- flag )
  size&limit < ;

: empty? ( addr -- flag )
  size 0= ;

: .heap ( addr -- addr )
  DUP DUP array SWAP size CELLS DUMP ;

: (push-back) ( n addr -- )
  TUCK DUP size 2 + CELLS + ! 1+size ;

: push-back ( addr n -- flag )
  SWAP DUP not-full? IF (push-back) -1 ELSE 2DROP 0 THEN ;

: parity-adjustment ( n -- n )
  DUP IF 1 AND 0= 1 AND THEN ;

: /2 ( n -- n )
  1 rshift ;

: *2 ( n -- n )
  1 LSHIFT ;

: parent ( n -- n )
  DUP /2 SWAP parity-adjustment - ;

: left-child ( n -- n )
  *2 1+ ;

: right-child ( n -- n )
  *2 2 + ;

: children ( n -- ln rn )
  left-child dup 1+ ;

: first ( addr -- n )
  0 SWAP @index ;

: last-index ( addr -- n )
  size 1 - ;

: last ( addr -- n )
  DUP last-index SWAP @index ;

: swap-addr ( addr addr -- )
  ( addr1 2 1 addr2 )
  2DUP @ SWAP @ ROT ! SWAP ! ;

: i->addr ( addr n1 n2 -- addr addr )
  CELLS SWAP CELLS ROT array TUCK + -ROT + ;

: swap-first&last ( addr -- )
  dup last-index 0 i->addr swap-addr ;

: swap-when ( addr n1 n2 -- flag )
  i->addr 2DUP compare IF swap-addr -1 ELSE 2DROP 0 THEN ;

\ refactor this junk!
: swap-left-child? ( addr n -- 0 | left-child -1 )
  >R R@ DUP left-child ROT SWAP 2DUP
  in-bounds? IF
    ROT SWAP-WHEN
  ELSE
    2DROP DROP 0
  THEN
  R> left-child SWAP DUP 0= IF
    SWAP DROP
  THEN ;

\ refactor this junk!
: swap-right-child? ( addr n -- 0 | right-child -1 )
  >R R@ DUP right-child ROT SWAP 2DUP
  in-bounds? IF
    ROT SWAP-WHEN
  ELSE
    2DROP DROP 0
  THEN
  R> right-child SWAP DUP 0= IF
    SWAP DROP
  THEN ;

\ refactor this junk!
: heap-sort-down ( addr n -- )
  SWAP >R
  BEGIN
    DUP R@ SWAP swap-left-child? 0= DUP IF
      DROP R@ SWAP swap-right-child? 0=
    ELSE
      ROT DROP
    THEN
  UNTIL
  R> DROP ;

: heap-sort-up ( addr n -- )
  SWAP >R
  BEGIN
    DUP parent TUCK R@ -ROT swap-when OVER AND 0=
  UNTIL
  R> 2DROP ;

: heap-add ( addr n -- flag )
  OVER not-full? IF
    SWAP TUCK SWAP push-back DROP DUP size 1- heap-sort-up -1
  ELSE 0
  THEN ;

\ Need to check empty? before calling this.
: heap-remove ( addr -- n )
  DUP DUP DUP first >R swap-first&last size-1 0 heap-sort-down R> ;

: @< ( addr addr -- flag )
  @ SWAP @ SWAP < ;

\ test comparison
' @< IS compare

: test 10 heap dup dup 8 heap-add drop 2 heap-add drop dup 12 heap-add drop dup 1 heap-add drop ;
